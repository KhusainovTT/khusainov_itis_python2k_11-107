import bs4
import multiprocessing
import os.path
import requests
from task12 import normalizer


images_url = []
where_to_save = 'images'
tuple_of_extensions = ('.jpg', '.png', '.img', '.jpeg')
tuple_of_protocols = ('http://', 'https://')


def domain_getter(url: str):
    domain = url
    for i in tuple_of_protocols:
        if i in domain:
            domain = url.replace(i, '')
    domain = domain.split('/')[0]
    return domain


def download_image(image_url):
    img_data = requests.get(image_url).content
    path_to_img = os.path.join(where_to_save, os.path.basename(image_url))
    os.makedirs(where_to_save, exist_ok=True)
    with open(path_to_img, 'wb') as handler:
        handler.write(img_data)
    print(f"Скачивается: {os.path.basename(image_url)}")


def url_getter(url: str):
    response = requests.get(url)
    domain = domain_getter(url)
    print(domain)
    tree = bs4.BeautifulSoup(response.text, 'html.parser')
    all_tags = tree.findAll('img')
    for tag in all_tags:
        img_url: str = tag.attrs.get("src")
        if img_url:
            if '?' in img_url:
                img_url = img_url[:img_url.index("?")]
            if img_url.endswith(tuple_of_extensions):
                img_url = normalizer(img_url, domain)
                images_url.append(img_url)


if __name__ == '__main__':
    site = input("Input site url:")
    url_getter(site)
    print("Image's address: ")
    print("\n".join(images_url))
    with multiprocessing.Pool() as pool:
        pool.map(download_image, images_url)
    print('Complete.')
