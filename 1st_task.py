a = float(input("Введите первое число: "))
b = float(input("Введите второе число: "))
operation = input("Введите операцию (+, -, /, *): ")

if operation == "+":
    result = a + b
elif operation == "-":
    result = a - b
elif operation == "/":
    result = a / b
elif operation == "*":
    result = a * b
else:
    print("Некорректная операция")
    result = None

if result is not None:
    print("Результат:", result)