import os

current_path = os.getcwd()


def pwd():
    print(current_path)


def cd(dir_path):
    global current_path
    if dir_path in os.listdir(current_path) or dir_path == "..":
        current_path = os.path.abspath(os.path.join(current_path, dir_path))
        print(current_path)
    else:
        print("There is no such directory")


def touch(filename):
    file_path = os.path.join(current_path, filename)
    with open(file_path, "w"):
        print(f"{file_path} created!")


def cat(filename):
    file_path = os.path.join(current_path, filename)
    if os.path.exists(file_path):
        with open(file_path, "r") as f:
            print(f.read())
    else:
        print("This file doesn't exist")


def ls():
    files_and_directories = os.listdir(current_path)
    print(*files_and_directories)


def rm(filename):
    file_path = os.path.join(current_path, filename)
    if os.path.exists(file_path):
        os.remove(file_path)
        print(f"File {file_path} deleted")
    else:
        print("The file you want to delete doesn't exist")


operations = {
    "pwd": pwd,
    "cd": cd,
    "touch": touch,
    "cat": cat,
    "ls": ls,
    "rm": rm
}

while True:
    command = input()
    commands = command.split()
    if len(commands) == 0 or len(commands) > 2:
        print("Wrong command")
        continue
    main_command = commands[0]
    if main_command in operations.keys():
        if len(commands) == 1:
            operations[main_command]()
        else:
            operations[main_command](commands[1:][0])
    else:
        print("Wrong command")
