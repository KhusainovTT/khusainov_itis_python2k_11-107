from math import prod

history = {"+": [], "-": [], "*": [], "/": []}  # словарь для хранения истории операций

while True:
    value = input("Введите значение (или 'exit' для выхода): ")

    if value == "exit":
        break

    try:
        value = float(value)
    except ValueError:
        print("Ошибка: введено некорректное значение")
        continue

    operation = input("Введите операцию (+, -, *, /): ")

    if operation in ["+", "-", "*", "/"]:
        history[operation].append(value)

        if operation == "+":
            result = sum(history["+"])
        elif operation == "-":
            result = value - sum(history["-"])
        elif operation == "*":
            result = value * prod(history["*"])
        elif operation == "/":
            try:
                result = value / prod(history["/"])
            except ZeroDivisionError:
                print("Ошибка: деление на ноль")
                continue

        print(f"Результат: {result}")
        print(f"История операций {operation}: {history[operation]}")
    else:
        print("Ошибка: введена некорректная операция")