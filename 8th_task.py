class Vector:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __add__(self, other):
        return Vector(self.x + other.x, self.y + other.y)

    def __sub__(self, other):
        return Vector(self.x - other.x, self.y - other.y)

    def __mul__(self, scalar):
        if isinstance(scalar, int) | isinstance(scalar, float):
            return Vector(self.x * scalar, self.y * scalar)

    def __str__(self):
        return f"({self.x},{self.y})"

if __name__ == '__main__':
    v1 = Vector(1, 2)
    v2 = Vector(3, 4)

    v3 = v1 + v2  # v3.x = 4, v3.y = 6
    v4 = v1 - v2  # v4.x = -2, v4.y = -2
    v5 = v1 * 2  # v5.x = 2, v5.y = 4

    print(v3, v4, v5)