import requests
import re


def get_info_by_ip(ip='127.0.0.1'):
    try:
        response = requests.get(url=f"http://ip-api.com/json/{ip}").json()
        print(response)
        if response.get('status') == 'success':
            data = {
                '[Country]': response.get('country'),
                '[Region]': response.get('regionName'),
                '[City]': response.get('city')
            }
            for i, j in data.items():
                print(f'{i} : {j}')
        else:
            print('[!] This IP does not exist')
    except requests.exceptions.ConnectionError:
        print('[!] Please check your connection')


def main():
    ip = input('Please enter a target IP: ')
    if bool(re.fullmatch(r"([\d]+\.){3}[\d]+", ip)):
        get_info_by_ip(ip=ip)
    else:
        print('[!] Incorrect IP')


if __name__ == '__main__':
    main()