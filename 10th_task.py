import requests
import bs4


def main():
    url = 'https://yandex.ru/pogoda/'
    response = requests.get(url)
    tree = bs4.BeautifulSoup(response.text, 'html.parser')
    week = tree.select(".forecast-briefly__day > a")
    current_day = week[1].attrs['aria-label']
    print(f"Погода {current_day}")
    print("Погода на неделю:")
    for day in week[2:9]:
        print(f"\t {day.attrs['aria-label']}")


if __name__ == '__main__':
    main()
