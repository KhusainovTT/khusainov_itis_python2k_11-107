import sys


def find_max(*args):
    max_num = -sys.maxsize
    for i in args:
        if i > max_num:
            max_num = i
    return max_num


def nums(*args):
    num = []
    for arg in args:
        try:
            num.append(int(arg))
        except ValueError:
            continue
    return num


def main():
    num = input("Введите числа через пробел.\n")
    n = nums(*num.split())
    max_num = find_max(*n)
    print(f"{max_num}")


if __name__ == '__main__':
    main()
