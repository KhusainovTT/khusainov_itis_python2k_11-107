def func():
    input_list = [x for x in input().strip().split()]
    answer_list = [int(x) for x in input_list if x.lstrip("-").isdigit()]

    print("Входные данные: ", *[x if x.lstrip("-").isdigit() else -1 for x in input_list])
    print("Чётные числа: ", *[x for x in answer_list if x % 2 == 0])
    print("Нечётные числа: ", *[x for x in answer_list if x % 2 == 1])
    print("Отрицательные числа: ", *[x for x in answer_list if x < 0])


if __name__ == '__main__':
    func()
