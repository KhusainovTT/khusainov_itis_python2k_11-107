import pytest


def united_url_of_python(url, path):
    return '/'.join([url.strip('/'), path])


def normalizer(url, domain):
    if url.startswith("//"):
        url = "https:" + url
    elif not url.startswith("http"):
        url = "https://" + domain + url
    return url


@pytest.mark.parametrize("link, domain, result", [
    ("//http.cat/200.jpg", "/200.jpg", "https://http.cat/200.jpg"),
    ("//http.cat/200.jpg", "http.cat", "https://http.cat/200.jpg"),
    ("https://http.cat/200.jpg", "http.cat", "https://http.cat/200.jpg")

])
def test_normalizer(link, domain, result):
    assert normalizer(link, domain) == result
