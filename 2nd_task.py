from math import prod

history = []  # список для хранения истории операций

while True:
    value = input("Введите значение (или 'exit' для выхода): ")

    if value == "exit":
        break

    try:
        value = float(value)
    except ValueError:
        print("Ошибка: введено некорректное значение")
        continue

    operation = input("Введите операцию (+, -, *, /): ")

    if operation == "+":
        result = sum(history + [value])
    elif operation == "-":
        result = value - sum(history)
    elif operation == "*":
        result = value * (1 if not history else prod(history))
    elif operation == "/":
        try:
            result = value / (1 if not history else prod(history))
        except ZeroDivisionError:
            print("Ошибка: деление на ноль")
            continue
    else:
        print("Ошибка: введена некорректная операция")
        continue

    history.append(value)

    print(f"Результат: {result}")
    print("История операций:", history)