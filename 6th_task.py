import sys
import random


def decorator(func):
    def output(*args, **kwargs):
        print(f"{func.__name__}: {args}, {kwargs}")
        answer = func(*args, **kwargs)
        print(f"{func.__name__}: {answer}")
        return answer
    return output


@decorator
def find_max(*args, **kwargs):
    max_num = -sys.maxsize
    for i in args:
        if i > max_num:
            max_num = i
    return max_num


if __name__ == '__main__':
    find_max(*[random.randint(-10000, 10000) for i in range(10)])
