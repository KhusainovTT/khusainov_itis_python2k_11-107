import re


def main():
    reg = r"(https\:\/\/([w]{3}\.|)youtu(be\.com\/watch\?v=|\be.\/)[\w-]{11}((\?|\&)t=[\d]+|))"
    text = input("Input link: ").strip()
    result = re.fullmatch(reg, text, re.MULTILINE)
    print(bool(result))


if __name__ == '__main__':
    main()
